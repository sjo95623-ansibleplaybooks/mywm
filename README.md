# MyWM

This role installs my full graphical environment on a linux machine.  
Currently supported distributions are:

- CentOS 7.7
- RHEL 7.6

# Installed roles

- geerlingguy.git (needed as a dependency)
- ansible-role-xrdp
- ansible-role-i3
- ansible-role-polybar
- ansible-role-dmenu

```
Todo:
- ansible-role-ranger
- ansible-role-xwallpaper
- ansible-role-pywal
- configs
```

# Variables

Here is a list of all the variables you can safely override :

```yaml
# Users which will be affected in case of local binairies install
# and which will get their configurations configured 
user_list:
  - jscherrer
  - jscherrerl

# If you need to go through an http proxy to download resources
# If you to bypass a restrictive proxy, see Proxying
http_proxy: "http://user:password@proxy.com/"

# This sets where to install all compiled libraries
prefix: /usr/local

# This sets $HOME/.local/bin as the first element in $PATH
# so you can use your local binaries before system ones
localbin_prefix_path: false
```

# Proxying 

## To set a simple http proxy
```yaml
http_proxy: "http://user:password@proxy.com/"
```

## To create a tunnel through a http proxy
```yaml
# Here you set the gateway address
tunnel: "user@remote_gateway -p <port>"
# Here you set an optionnal http proxy address
# do not prefix with http://
http_proxy: "<proxy_url>:<port>"
```
A local http_proxy forwarder will be created thanks to privoxy and be used (`127.0.0.1:8118`)

## To create a tunnel through a socks5 proxy
```yaml
# Here you set the gateway address
tunnel: "user@remote_gateway -p <port>"
# Here you set an optionnal socks proxy address
socks_proxy: "<proxy_url>:<port>"
```
A local http_proxy forwarder will be created thanks to privoxy and be used (`127.0.0.1:8118`)
